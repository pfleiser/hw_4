//Sources: https://mathcs.clarku.edu/~djoyce/cs101/Resources/Fraction.java
//https://www.youtube.com/watch?v=EWRs7Sh7cIU

import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b < 1) {
   throw new RuntimeException("Denominator can't be 0 or less");
      }
      this.numerator = a;
      this.denominator = b;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return ""+Long.toString(this.numerator)+"/"+Long.toString(this.denominator);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   
   @Override
   public boolean equals (Object m) {
      Lfraction other = (Lfraction) m;
      if (other.denominator == this.denominator) {
         return this.numerator == other.numerator;
      } else {
         double dif;
         if (other.denominator > this.denominator) {
            dif = other.denominator / this.denominator;
         } else {
            dif = this.denominator / other.denominator;
         }
         return (this.numerator*dif) == other.numerator;
      }

   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.numerator, this.denominator);
   }

   public Lfraction decrease() {
      long common;
      if (this.numerator == this.denominator) {
         common = this.numerator;
      } else {
         long de1;
         long de2;

         if (this.numerator > this.denominator) {
            de1 = this.numerator;
            de2 = this.denominator;
         } else {
            de1 = this.denominator;
            de2 = this.numerator;
         }

         long f;
         while (de2 != 0) {
            f = de2;
            de2 = de1 % de2;
            de1 = f;
         }
         common = de1;
      }
      return new Lfraction(this.numerator / common, this.denominator / common);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      if (this.denominator == m.denominator) {
         return new Lfraction(this.numerator + m.numerator, this.denominator);
      } else {
         return new Lfraction((m.numerator*this.denominator)+(this.numerator*m.denominator), (m.denominator*this.denominator));
      }
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(this.numerator * m.numerator, this.denominator * m.denominator).decrease();
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.numerator < 1) {
         return new Lfraction(-this.denominator, -this.numerator);
      } else {
         return new Lfraction(this.denominator, this.numerator);
      }

   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.numerator, this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      if (this.denominator == m.denominator) {
         return new Lfraction(this.numerator - m.numerator, this.denominator);
      } else {
         return new Lfraction((this.numerator*m.denominator)-(m.numerator*this.denominator), (m.denominator*this.denominator)).decrease();
      }
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      long numerator = Math.abs(this.numerator * m.denominator);
      long denominator = Math.abs(this.denominator * m.numerator);
      return new Lfraction(numerator, denominator).decrease();
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (this.equals(m)) {
         return 0;
      } else if (this.numerator * m.denominator  < m.numerator * this.denominator) {
         return -1;
      } else {
         return 1;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerator/this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      if (this.numerator >= this.denominator || -this.numerator >= this.denominator) {
         return new Lfraction(this.numerator % this.denominator, this.denominator);
      } else {
         return new Lfraction(this.numerator, this.denominator);
      }
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)this.numerator/this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] parts = s.split("/");
      return new Lfraction(Long.parseLong(parts[0]), Long.parseLong(parts[1]));
   }
}